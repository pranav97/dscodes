/*
**Program to implement Circular Queue
**using an array
*/

#include<stdio.h>
#include<stdlib.h>

int front=-1,rear=-1,*q,size,MAX_SIZE;    //globally declare some variables to use in functions

/*
** Front is to show the start of the queue, that is the variable that 
** will be used to dequeue. 
** Rear is to show the last element in the queue and that is helpful
** to dequeue
** *q is the pointer variable for our queue to be used in malloc
*/

void enq(void );           //function declaration for Enqueueing 
void deq(void );	  //function declaration for dequeueing 
void display(void );     //function declaration for displaying the queue
void options(void );    //function declaration to choose our option

//Main function

int main()
{
	printf("\nEnter the maximum size possible for the queue\n");
	scanf("%d",&MAX_SIZE);
	printf("Enter the present size of the queue that should be created\n");
	scanf("%d",&size);
	if(size > MAX_SIZE)             //sanity check
	{
		printf("size of the queue should not exceed it's limit\n");
		return 0;
	}
	q=(int*)malloc(size*sizeof(int));  //Dynamically allocate memory for the queue

	//First time enqueue

	printf("Now enter the elements in the queue\n");
	front++;
	rear++;
	for(rear=0;rear<size;rear++)
	{
		scanf("%d",&q[rear]);
	}
	rear--;
	display();
	options();
}

//Definitions for the various functions used in this program

void options()
{
	int a=0;
	printf("\n|=============================================================|\n");
	printf("|Press 1 to display the queue                                 |\n");
	printf("|Press 2 to enqueue an element to the queue                   |\n");
	printf("|Press 3 to dequeue an element from the queue                 |\n");
	printf("|Press ctrl+z to exit                                         |\n");
	printf("|=============================================================|\n");
	scanf("%d",&a);
	if(a==1)
	display();
	else if(a==2)
	enq();
	else if(a==3)
	deq();
	else
	printf("You have entered a wrong choice, try again\n");
	options();
}

void display()
{
	int i;
	if(front == -1 && rear == -1)
	{
		printf("Queue is empty\n");
		options();
	}
	for(i=front;i<=rear;i++)
	printf("%d\t",q[i]);
}

void enq()
{
	int a;
/*
** If the queue has reached maximum size already
** then show the error and leave this go out of 
** this function. Else add the input element
*/
	if(rear == MAX_SIZE-1 && front = 0 || front==rear+1)
	printf("Queue Limit Reached\n");
	else if(front == -1)
	{
		front++;
		rear=(rear+1)%Max_SIZE;
		printf("Enter the element to be pushed\n");
		scanf("%d",&q[rear]);
	}
}

void deq()
{

/*
** If the queue is already empty then show that to the user and go out of
** the function, else if there is only one element, print the element and
** reset front,rear to -1 to avoid issues else just display element and  
** increment front 
*/
	if(front == -1)
	printf("Queue is empty\n");
	else if(front==rear)
	{
		printf("%d element has been dequeued\n",q[front]);	
		front=-1;
		rear=-1;
	}
	else
	{
		front=(front+1)%MAX_SIZE;
		printf("%d element has been dequeued\n",q[front++]);
	}		
}
