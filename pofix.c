/*
** Program to convert infix to postfix
** Note: Refer eval.c for evaluation of postfix and
**       for further details
*/

#include<stdio.h>
#include<stdlib.h>
#include<ctype.h>


#define SIZE 50
int top=-1;
char s[SIZE];

char pop()
{
	return (s[top--]);
}

push(char elem)
{
	s[++top]=elem;
}

int pr(char elem)
{
	if(elem == '$')
	return 0;
	else if(elem == '(')
	return 1;
	else if(elem == '+' || elem == '-')
	return 2;
	else if(elem == '*' || elem == '/')
	return 3;
	else if(elem == '^')
	return 4;
}

int main()
{
	char p,ch,infix[50],postfix[50],elem;
	int i=0,k=0;
	printf("Enter the infix now\n");
	scanf("%s",infix);
	push('$');
	while((ch=infix[i++])!= '\0')
	{
		if(ch == '(')
		push(ch);
		else if(isalnum(ch))
		postfix[k++]=ch;
		else if(ch == ')')
		{
			while(s[top]!='(')
			postfix[k++]=pop();
			p=pop();
		}
		else
		{
			while(pr(s[top]) >= pr(ch))
			postfix[k++]=pop();
			push(ch);
		}
	}
	while(s[top]!= '$')
	postfix[k++]=pop();
	postfix[k]='\0';
	printf("The following is the entered postfix\n");
	printf("%s",postfix);
	printf("\n");
	return 0;
}
