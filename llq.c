/*
** Program to implement a queue using linked list
** Refer to link.c and q.c for more info
*/

#include<stdio.h>
#include<stdlib.h>

struct node{
	int data;
        struct node *next;
}*front=NULL,*rear=NULL,*n;

void options(int);
void display();
void enq();
void deq();

int main()
{
	options(-1023);
	return 0;
}

void options(int a)
{

	printf("\n|--------------------------------------------|\n");
	printf("|Press 1 to display the queue                |\n");
	printf("|Press 2 to enqueue an element               |\n");
	printf("|Press 3 to dequeue an element               |\n");
	printf("|Press ctrl+z to exit                        |\n");
	printf("|--------------------------------------------|\n");
	scanf("%d",&a);
	if(a==1)
	display();
	else if(a==2)
	enq();
	else if(a==3)
	deq();
}

void enq()
{
	n = (struct node*)malloc(sizeof(struct node));
	printf("Enter the element\n");
	scanf("%d",&n->data);
	if(front == rear && front == NULL)
	{
		front=n;
		rear=n;
	}
	else
	{
		rear->next=n;
		rear=rear->next;
	}
	options(-1023);
}

void display()
{
	struct node *current;
	current = front;
	if(front == rear && front == NULL)
	{
		printf("Queue is empty\n");
	}
	while(current!=NULL)
	{
		printf("%d\t",current->data);
		current=current->next;
	}
	options(-1023);
}
void deq()
{
	struct node *current;
	current = front;
	if(front == NULL)
	{
		printf("Queue is empty\n");
	}
	else
	{
		printf("%d has been dequeued\n",current->data);
		front=front->next;
		current->next=NULL;
	}
	options(-1023);	
}
