/*
**Program to implement a Doubly Linked List
*/

#include<stdio.h>
#include<stdlib.h>

/*
** A linked list is a structure that has two individual data types.
** A variable to hold the value in the node
** A variable that points to the next node
*/

struct node{
	int data;
	struct node *next;
};

struct node *head = NULL;

void options();              //Function to show various options in the program
void create();		    //Function to create a node
void display();            //Function to display the list
void insert();            //Function to insert a new node
void delete();           //Function to delete a node
void search();          //Function that searches for a particular element         
int count();           //Function that returns the number of nodes
void sort();          //Function that sorts in ascending order         
void reverse();      //Function that reverses the linked list

int main()
{
	options();
	return 0;
}

void options()
{

	int a=0;
	printf("\n");
	printf("|===========================================================|\n");
	printf("|Press 1 to display the list                                |\n");
	printf("|Press 2 to create a node at beginning or end               |\n");
	printf("|Press 3 to insert an element after a desired position      |\n");
	printf("|Press 4 to delete an element at a position                 |\n");
	printf("|Press 5 to search for a particular element                 |\n");
	printf("|Press 6 to get the number of nodes in the list             |\n");
	printf("|Press 7 to sort the linked list                            |\n");
	printf("|Press 8 to reverse the linked list                         |\n");
	printf("|===========================================================|\n");
	scanf("%d",&a);
	if(a==1)
	display();
	else if(a==2)
	create();
	else if(a==3)
	insert();
	else if(a==4)
	delete();
	else if(a==5)
	search();
	else if(a==6)
	printf("%d number of elements were present\n",count());
	else if(a==7)
	sort();
	else if(a==8)
	reverse();
	options();
}

void create()
{
	printf("\n");	
	struct node *current,*new;
	char ch='\0';
	new=(struct node *)malloc(sizeof(struct node *));
	new->next=NULL;
	printf("Enter the value of the element\n");
	scanf("%d",&new->data);
	current=head;
	if(current == NULL)            //Adressing empty list
	{
		current=new;
		head=new;
	}
	else                          //Adressing non empty list
	{
		printf("Do you want to add at beginning or add at end: (b/e)?");
		scanf("%s",&ch);
		if(ch == 'e')
		{
			current = head;
			while(current->next!=NULL)
			current=current->next;
			current->next=new;
		}
		else if(ch == 'b')
		{
			new->next=head;
			head=new;
		}
	}
}

void display()
{
	printf("\n");
	struct node *current;
	current=head;
	while(current!=NULL)
	{
		printf("%d\t",current->data);
		current=current->next;
	}
}

void insert()
{
	int pos,i=1;
	struct node *current,*temp,*new;
	new=(struct node*)malloc(sizeof(struct node *));
	new->next=NULL;
	current=head;
	printf("Enter the value of the element\n");
	scanf("%d",&new->data);
	printf("Enter the location after which you want an element\n");
	scanf("%d",&pos);
	for(i=1;i<pos;i++)
	current=current->next;  //Reach the required position
	temp=current->next;    //take temp to next next node
	current->next=new;     //Link newly created node to it's previous
	new->next=temp;        //Link newly created node to it's next	
}

void delete()
{
	int pos,i=1;
	struct node *current;
	current=head;
	printf("Enter the location at which you want to delete\n");
	scanf("%d",&pos);
	if(pos == 1)
	{
		printf("WARNING: DO NOT DELETE THE HEAD (FIRST) ELEMENT\n");
		main();
	}
	for(i=1;i<pos-1;i++)
	current=current->next;  //Reach the required position
	current->next=current->next->next;	
}

void search()
{
	int skey,i=1;
	struct node *current;
	current = head;
	printf("What do you want to search for?\n");
	scanf("%d",&skey);
	while(current!=NULL)
	{
		if(current->data==skey)
		{
			printf("Element is present at location %d \n",i);
			options();
		}
		else
		i++;
		current=current->next;
	}
	printf("Element not found\n");
}

int count()
{
	int i=1;
	printf("\n");
	struct node *current;
	current=head;
	while(current!=NULL)
	{
		i++;
		current=current->next;
	}
	return i-1;
}

void sort()
{

/*
** USE THA BUBBLLEEE SORRTTTT 
** COZ ITS EASY
*/
	struct node *temp1,*temp2;
	int dummy;
	temp1=NULL;
	temp2=NULL;
	for(temp1=head;temp1!=NULL;temp1=temp1->next)
	{
		for(temp2=temp1->next;temp2!=NULL;temp2=temp2->next)
		{
			if(temp1->data > temp2->data)
			{
				dummy=temp1->data;
				temp1->data=temp2->data;
				temp2->data=dummy;
			}
		}
	}
}

void reverse()
{
	struct node *temp1,*temp2,*current;
	int dummy,i=1,p,j=1;
	current=head;
	temp1=head; //value at first node
	p=count();
	while(i!=p)
	{
		current=current->next;
		i++;
	}
	temp2=current;//value at last node
	//Attempt to keep swapping the first with last, second with last but one etc
	for(i=1;i<=p/2;i++)
	{
		dummy=temp1->data;
		temp1->data=temp2->data;
		temp2->data=dummy;
		temp1=temp1->next;
		current=head;
		j=i;
		while(j!=(p-i))
		{
			current=current->next;
			j++;
		}
		temp2=current;							
	}
	display();	
}
