/*
** This is a program that adds two given polynomials
** using linked list to contain the polynomials
*/

#include<stdio.h>
#include<stdlib.h>

/*
** The following structure is a node that has 
** an integer that is a coefficient, an integer
** that is the exponent and a next pointer
*/

struct node{
	int coeff;
	int expo;
	struct node *next;
}*start1=NULL,*start2=NULL;       //start1 and start2 point to the first elements in each of the lists


void create(void);          //function to create the polynomials
void display(void);        //function to display the polynomials
void options(void);       //function that lists all available options
void add(void);          //function that adds both of the polynomials

int main()
{
	options();
	return 0;
}


void options()
{
	int d;
	printf("\n");
	printf("|======================================================|\n");
	printf("|Press 1 to display both of the polynomials            |\n");
	printf("|Press 2 to create the polynomials                     |\n");
	printf("|Press 3 to add the polynomials                        |\n");
	printf("|======================================================|\n");
	scanf("%d",&d);
	if(d==1)
	display();
	else if(d==2)
	create();
	else if(d==3)
	add();
	else
	printf("Entered wrong options. Try again\n");
	options();
}

void display()
{
	struct node *current1,*current2;
	current1=start1;
	current2=start2;
	printf("The following is the first polynomial\n");
	while(current1!=NULL)          
	{
		printf("(%dx^%d) + ",current1->coeff,current1->expo);
		current1=current1->next;
	}
	printf ("\b\b \n"); //\b\b to erase the last + sign
	printf("\nThe following is the second polynomial\n");
	while(current2!=NULL)       
	{
		printf("(%dx^%d) + ",current2->coeff,current2->expo);
		current2=current2->next;
	}
	printf ("\b\b \n"); //\b\b to erase the last + sign
}

void create()
{
	struct node *new1,*new2,*current1,*current2;
	int ch;
	new1=(struct node*)malloc(sizeof(struct node*));
	new2=(struct node*)malloc(sizeof(struct node*));
	printf("First or Second Polynomial (1/2):  ");
	scanf("%d",&ch);
	if(ch==1)
	{
		current1=start1;
		printf("Enter the coefficient\n");
		scanf("%d",&new1->coeff);
		printf("Enter the exponent of x\n");
		scanf("%d",&new1->expo);
		if(start1==NULL)       //adress empty polynomial 1
		{
			start1=new1;
			new1->next=NULL;
		}
		else              //adress non empty polynomial 1
		{
			current1=start1;
			while(current1->next!=NULL)
			current1=current1->next;
			current1->next=new1;
			new1->next=NULL;
		}
	}
	else if(ch==2)
	{
		{
			current2=start2;    
			printf("Enter the coefficient\n");
			scanf("%d",&new2->coeff);
			printf("Enter the exponent of x\n");
			scanf("%d",&new2->expo);
			if(start2==NULL)       //adress empty polynomial 2
			{
				start2=new2;
				new2->next=NULL;
			}
			else              //adress non empty polynomial 2
			{
				current2=start2;
				while(current2->next!=NULL)
				current2=current2->next;
				current2->next=new2;
				new2->next=NULL;
			}
		}
	}
}

void add()
{
		struct node *current1,*current2;
		int i=0;
		current1=start1;
		current2=start2;
		//Add The Coefficients Of All Common Exponents
		while(current1!=NULL)
		{
			current2=start2;
			while(current2!=NULL)
			{
				if(current1->expo==current2->expo)
				{
					printf("(%dx^%d) + ",current1->coeff+current2->coeff,current1->expo);
					break;
				}
				else
				current2=current2->next;
			}
			current1=current1->next;
		}
		//Print the Coefficients Present in Polynomial 1 and not in Polynomial 2
		current1=start1;
		current2=start2;
		while(current1!=NULL)
		{
			i=0;
			current2=start2;
			while(current2!=NULL)
			{
				if(current1->expo==current2->expo)
				{
					i++;
					break;
				}
				else
				current2=current2->next;
			}
			if(i==0)
			printf("(%dx^%d) + ",current1->coeff,current1->expo);
			current1=current1->next;
		}
		//Print the Coefficients Present in Polynomial 2 and not in Polynomial 1
		current1=start1;
		current2=start2;
		while(current2!=NULL)
		{
			i=0;
			current1=start1;
			while(current1!=NULL)
			{
				if(current2->expo==current1->expo)
				{
					i++;
					break;
				}
				else
				current1=current1->next;
			}
			if(i==0)
			printf("(%dx^%d) + ",current2->coeff,current2->expo);
			current2=current2->next;
		}
		printf ("\b\b \n"); //\b\b to erase the last + sign
}
