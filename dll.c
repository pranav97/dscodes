/*
**Program to implement a Doubly Linked List
*/

#include<stdio.h>
#include<stdlib.h>

void options(void);              //Function to show various options in the program
void create(void);		//Function to create a node
void display(void);            //Function to display the list
void insert(void);            //Function to insert a new node
void delete(void);           //Function to delete a node
int count(void);            //Function that returns the number of nodes

/*
** A doubly linked list is a structure that has three individual data types.
** A variable to hold the value in the node
** A variable that points to the next node
** A variable that points to the previous node
*/

struct node{
	int data;		
	struct node *next;     
	struct node *prev;    	
};

struct node *start=NULL;       //start node that always points to the first element in the list

int main()
{
	options();
	return 0;
}

void options()
{
	int a;
	printf("\n");
	printf("|====================================================|\n");
	printf("|Press 1 to display the list                         |\n");
	printf("|Press 2 to add elements to the list(beggining/end)  |\n");
	printf("|Press 3 to insert an element at a position          |\n");
	printf("|Press 4 to delete an element                        |\n");
	printf("|====================================================|\n");
	printf("\n");
	scanf("%d",&a);
	if(a==2)
	create();
	if(a==1)
	display();
	if(a==3)
	insert();
	if(a==4)
	delete();
	options();            //Call options again irrespective of what was called before
}

void create()
{
	struct node *new,*current;
	char ch;
	current=start;
	new=(struct node*)malloc(sizeof(struct node*));
	printf("Enter the value of the element to be added in list\n");
	scanf("%d",&new->data);
	if(start==NULL)           //Adressing Empty List
	{
		start=new;
		new->prev=NULL;
		new->next=NULL;
	}
	else                          //Adressing Non Empty List
	{
		printf("Add at beginning or add at end or in middle (b/e): ");
		scanf("%s",&ch);
		if(ch=='e')               //Create element at beginning
		{
			current=start;
			while(current->next!=NULL)
			current=current->next;
			current->next=new;
			new->prev=current;
			new->next=NULL;
		}
		else if(ch=='b')           //Create element at end
		{
			current=start;
			current->prev=new;
			new->prev=NULL;
			new->next=start;
			start=new;
		}		
	}
}

void insert()
{
	int pos,i=1;
	struct node *current,*new;
	new=(struct node*)malloc(sizeof(struct node*));
	current=start;
	printf("Enter the position at which you want a new element\n");
	scanf("%d",&pos);
	printf("Enter the value of the element\n");
	scanf("%d",&new->data);
	if(pos==1)       //Insert  at fist position
	{
		if(start==NULL)            //Adressing empty list
		{
			start=new;
			new->prev=NULL;
			new->next=NULL;
		}
		else                     //Adressing non empty list
		{
			current=start;
			new->next=current;
			current->prev=new;
			new->prev=NULL;
			start=new;
		}	
	}
	else if(pos<0)                   //Incorrect Indexing
	{
		printf("Invalid position\n");
		options();
	}
	else
	{
		while(i!=(pos-1))      //Insert element at position greater than 1
		{
			current=current->next;
			i++;
		}
		new->next=current->next;
		current->next=new;
		new->prev=current;
	}
}

void delete()
{
	char ch;
	int pos,i=1,last;
	struct node *current;
	current=start;
	last=count();
	if(start == NULL)               //No Elements
	{
		printf("List empty, add elemets\n");
		options();
	}
	printf("Do you want to delete first/last/element at a position?\n");
	printf("f/l/p:");
	scanf("%s",&ch);
	if(ch == 'f')
	{
		current=start;
		if(current->next == NULL)    	//Only One Element
		{
			current = NULL;
			start = NULL;
		}
		else  				//More Than One Element
		{
			start=current->next;
			current->next=NULL;
			start->prev=NULL;
		}				
	}
	else if(ch == 'l')
	{
		current=start;
		if(current->next == NULL)    	//Only One Element
		{
			current = NULL;
			start = NULL;
		}
		else  				//More Than One Element
		{
			while(current->next!=NULL)
			current=current->next;
			current->prev->next=NULL;			
		}		
	}
	else if(ch == 'p')
	{
		printf("Enter the position at which you want to delete the element");
		scanf("%d",&pos);
		if(pos == 1)              //Deleting the first element
		{
			current=start;
			if(current->next == NULL)    	//Only One Element
			{
				current = NULL;
				start = NULL;
			}
			else  				//More Than One Element
			{
				start=current->next;
				current->next=NULL;
				start->prev=NULL;
			}			
		}
		if(pos == last)  //last
		{
			i=1;
			current=start;
			while(i<=(last-2))
			{
				current=current->next;
				i++;
			}
			current->next=NULL;
		}
		else             
		{
			current=start;
			while(i!=(pos-1))
			current=current->next;
			current->next->next->prev=current;
			current->next=current->next->next;
		}
	}
}

void display()
{
	struct node *current;
	current=start;
	if(start==NULL) 			//No Elements
	{
		printf("List empty, add elements\n");
		options();
	}
	while(current!=NULL)
	{
		printf("%d\t",current->data);
		current=current->next;
	}
}

int count()
{
	int i=1;
	struct node *current;
	current=start;
	if(start==NULL) 			//No Elements
	return 0;
	while(current!=NULL)
	{		
		current=current->next;
		i++;
	}
	return i-1;
}
