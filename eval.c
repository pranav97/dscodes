/*
** Program to convert the input infix to postfix and
** then to evaluate the final postfix
** Note: Compile with 'cc eval.c -lm'
*/

#include<stdio.h>
#include<math.h>
#include<ctype.h>
#include<string.h>

char s[50];          //Stack as an intermediary for conversion
int s1[50];          //Stack as an intermediary for evaluation
int top=-1;          //variable that always points to the top of a stack

/*
** The following are various functions created to help 
** pushing/popping to the two above stacks,convert to
** and evaluate the final postfix
*/

int pop()
{
	return s[top--];
}

void push1(int ele)
{
	top++;
	s1[top]=(int)(ele-48);
}

void eval(char ele)
{
	int a,b,res;
	a=s1[top];
	s1[top]='\0';
	top--;
	b=s1[top];
	s1[top]='\0';
	top--;
	if(ele == '+')
	res=b+a;
	else if(ele == '-')
	res=b-a;
	else if(ele == '*')
	res=b*a;
	else if(ele == '/')
	res=b/a;
	else if(ele == '^')
	res=pow(b,a);
	top++;
	s1[top]=res; 	
}

void push(int ele)
{
	s[++top]=ele;
}

int pr(char a)
{
	if(a == '$')
	return 0;
	else if(a == '(')
	return 1;
	else if(a == '+' || a == '-')
	return 2;
	else if(a == '*' || a =='/')
	return 3;
	else if(a == '^')
	return 4;
}

int main()
{
	char infix[50],postfix[50],p,ch,elem;
	int k=0,i=0;
	printf("Enter the infix\n");
	scanf("%s",infix);
	push('$');                            //dummy to let us know the first element in the stack
	while((ch=infix[i++])!='\0')          //keep taking elements from infix into ch until the end
	{
		if(ch == '(')                  
		push(ch);
		else if(isalnum(ch))              //operand
		postfix[k++]=ch;
		else if(ch == ')')
		{
			while(s[top]!= '(')
			postfix[k++]=pop();
			p=pop();
		}
		else                             //operator
		{		
			while((pr(s[top]) >= pr(ch)))        //higher operator will be popped to postfix
			postfix[k++]=pop();
			push(ch);
		}
	}
	while(s[top]!= '$')
	postfix[k++]=pop();          //Take the rest of operands into the final postfix
	postfix[k]='\0';
	printf("The following is the entered postfix\n");
	printf("%s",postfix);
	printf("\n");
	top=-1;
	for(i=0;i<strlen(postfix);i++)
	{
		if(postfix[i] >= '0' && postfix[i] <= '9')       //check for operands
		push1(postfix[i]);
		if(postfix[i] == '+' || postfix[i] == '-' || postfix[i] == '*' || postfix[i] == '/' || postfix[i] == '^')   //check for operators
		eval(postfix[i]);
	}
	printf("The evaluated result of postfix is %d\n",s1[top]);
	return 0;
}
