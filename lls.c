/*
** Program to implement a stack using linked list
** Refer to link.c and stack.c for more info
*/

#include<stdio.h>
#include<stdlib.h>

struct node{
	int data;
	struct node *next;
};

struct node *top = NULL;
struct node *new = NULL;
struct node *current = NULL;

void push();
void pop();
void display();
void options(int);

int main()
{
	options(-1023);
	return 0;
}


void options(int a)
{

	printf("\n|--------------------------------------------|\n");
	printf("|Press 1 to display the stack                |\n");
	printf("|Press 2 to push an element to the stack     |\n");
	printf("|Press 3 to pop an element to the stack      |\n");
	printf("|Press ctrl+z to exit                        |\n");
	printf("|--------------------------------------------|\n");
	scanf("%d",&a);
	if(a==1)
	display();
	else if(a==2)
	push();
	else if(a==3)
	pop();
}

void push()
{
	int n;
	new=(struct node *)malloc(sizeof(struct node));
	printf("Enter the element you want to be pushed\n");
	scanf("%d",&n);
	new->data=n;
	new->next=NULL;
	if(top == NULL)
	{
		top=new;
	}
	else
	{
		new->next=top;
		top=new;
	}
	options(-1023);	
}

void pop()
{
	if(top == NULL)
	{
		printf("Stack is empty\n");
		options(-1023);
	}
	else
	{
		printf("%d has been popped out\n",top->data);
	}
	top=top->next;
	options(-1023);
}

void display()
{
	struct node *current;
	current=top;
	if(current == NULL)
	printf("List Empty, add elements\n");
	while(current!=NULL)
	{
		printf("%d\t",current->data);
		current=current->next;
	}
	options(-1023);
}
