/*
** Program to implement a Stack
** using an array.
** A stack is a LIFO type of data structure. 
** Last element that is In is First Out. 
*/


#include<stdio.h>
#include<stdlib.h>

void display();          //function to display the stack
void pop();             //function to remove the topmost element
void push();           //function to add a new element on top
void options();

int *top,size,max_size,*data,dummy;

int main()
{
	int i;
	printf("Enter the maximum size of the stack\n");
	scanf("%d",&max_size);
	data=(int*)malloc(max_size*sizeof(int));
	printf("Enter the size of the array now\n");
	scanf("%d",&size);
	if(size>max_size)           //sanity check
	{
		printf("Size cannot be greater than the maximum defined\n");
		return 0;
	}
	else                      //first time filling of the stack
	{
		printf("Fill the stack now\n");
		for(i=0;i<size;i++)
		{
			scanf("%d",&data[i]);
		}
		top=&dummy;
		*top=size-1;
		display();
		options();
	}	
}

void options()
{
	int a;
	printf("\n");
	printf("|============================================|\n");
	printf("|Press 1 to display                          |\n");
	printf("|Press 2 to push an element                  |\n");
	printf("|Press 3 to pop an element                   |\n");
	printf("|Press ctrl+z to exit                        |\n");
	printf("|============================================|\n");
	printf("\n");
	scanf("%d",&a);
	if(a==1)
	display();
	else if(a==2)
	push();
	else if(a==3)
	pop();
	else
	{
		printf("You have entered a wrong choice. Try again\n");
		options();
	}	
}

void display()
{
	printf("\n");
	int i;
	for(i=0;i<=*top;i++)
	printf("%d\t",data[i]);
	options();
}

void push()
{
	int a;
	if(*top==(max_size-1))          //Stack Overflow
	printf("Stack is full\nConsider popping\n");
	else
	{
		printf("Enter the element you are about to push\n");
		scanf("%d",&a);
		*top=*top+1;
		data[*top]=a;
		printf("%d has been pushed to the stack\n",data[*top]);
	}
	options();
}

void pop()
{
	if(*top<0)                     //Stack Underflow
	{
		printf("The stack is empty\n");
	}
	else
	{
		printf("%d element has been popped\n",data[*top]);
		*top=*top-1;
	}
	options();
}
